# check-upsmib plugin

This is a Nagios and icinga compatible plugin for monitoring UPSes that are compatible with the UPS-MIB using SNMP version 1, 2c or 3.

```
USAGE
  check_upsmib -T CHECK_TYPE -H HOST -C COMMUNITY [OPTIONS]
  check_upsmib -T CHECK_TYPE -H HOST -s 3 -u USERNAME -A AUTHPASSWORD [OPTIONS]
```

## Command line Options

```
OPTIONS
  -T CHECK_TYPE, --checktype=CHECK_TYPE
  -H HOST, --hostname=HOST
  -p PORT, --port=PORT
  -s SNMP_VERSION, --snmpversion=SNMP_VERSION   [1|2|3]
  -w THRESHOLD, --warning=THRESHOLD
  -c THRESHOLD, --critical=THRESHOLD
  -h, --help
  -V, --version
SNMP Version 1 or 2c specific
  -C COMMUNITY, --community=COMMUNITY
SNMP Version 3 specific
  -a PROTOCOL, --protocol=PROTOCOL   [md5|sha]
  -A AUTHPASSWORD, --authpassword=AUTHPASSWORD
  -u USERNAME, --username=USERNAME
```

## Functionality

This plugin can be used to alert on the following:

- **AlarmsPresent** - this reports the present number of active alarm conditions on the UPS, normal should report 0 (zero).
- **BatteryStatus** - The status of the battery: unknown(1), batteryNormal(2), batteryLow(3), batteryDepleted(4)
- **BatteryTemperature** - The temperature of the battery in degrees centigrade
- **EstimatedChargeRemaining** - An estimate of the battery charge remaining expressed as a percent of full charge.
- **EstimatedMinutesRemaining** - An estimate of the time to battery charge depletion under the present load conditions.
- **OutputSource** - The present source of output power: other(1), none(2), normal(3), bypass(4), battery(5), booster(6), reducer(7)

Note: For OutputSource, WARNING and CRITICAL return statuses are hard coded and cannot be set with the command line options.

## Example Usage

Performing checks against a ups with an ip address of ```10.1.1.10```
and an SNMP community name of ```community``` 
Each check type is performed seperately e.g.

```
check_upsmib -H 10.1.1.10 -C community -T AlarmsPresent
check_upsmib -H 10.1.1.10 -C community -T BatteryStatus
check_upsmib -H 10.1.1.10 -C community -T BatteryTemperature
check_upsmib -H 10.1.1.10 -C community -T EstimatedChargeRemaining
check_upsmib -H 10.1.1.10 -C community -T EstimatedMinutesRemaining
check_upsmib -H 10.1.1.10 -C community -T OutputSource
```

## Performance Data

Each of the checks outputs performance data. This can be processed by nagios/icinga to produce graphs.

## Setting Critical and Warning Thresholds

This plugin will accept warning and critical options for each of the check types. Where these
are not defined, sensible defaults exist. Below details the defaults that get used when the critical
and warning thresholds are not passed to the plugin:

Below are the default and warning thresholds for each check:

- **AlarmsPresent**
  - WARNING: n/a 
  - CRITICAL: 1
- **BatteryStatus**
  - WARNING: 3
  - CRITICAL: 4
- **BatteryTemperature**
  - WARNING: 26
  - CRITICAL: 30
- **EstimatedChargeRemaining**
  - WARNING: 50
  - CRITICAL: 30
- **EstimatedMinutesRemaining**
  - WARNING: 25
  - CRITICAL: 15
- **OutputSource**
  - WARNING and CRITICAL return statuses are hard coded and cannot be set with the command line options.

Below is an example where critical and warning thresholds are set for BatteryTemperature. Warning at 25 degrees
celcius and critical at 30 degrees celcius.

```
./check_upsmib -H 10.1.1.10 -C community -T BatteryTemperature -w 25 -c 30
```

Below is an example using snmp version 3.
```
./check_upsmib -H 10.1.1.10 -s 3 -u myusername -A mysecretpassword -T BatteryTemperature
```

## Testing

- This plugin was developed and tested with
  - A Chloride AM-P3-CN UPS.
  - An ABB Powerscale with a CS141 SNMP/WEB Adapter

### Using snmpget for troubleshooting
This process tests SNMP connectivity without using nagios or the check_upsmib script and is good for verifying the snmp server setup on your UPS independantly of nagios. This command queries all of the values that the check_upsmib script queries.

On debian or ubuntu install the snmp package.
```
apt install snmp
```
Then query your UPS directly with
```
snmpget -v 2c -c <your_snmp_string> <your_ups_ip_addr> .1.3.6.1.2.1.33.1.1.5.0 .1.3.6.1.2.1.33.1.6.1.0 .1.3.6.1.2.1.33.1.2.1.0 .1.3.6.1.2.1.33.1.2.7.0 .1.3.6.1.2.1.33.1.2.4.0 .1.3.6.1.2.1.33.1.2.3.0 .1.3.6.1.2.1.33.1.2.2.0 .1.3.6.1.2.1.33.1.4.1.0
```
You should see values returned for each of the 8 mib values queried. Output should be similar to below:
```
SNMPv2-SMI::mib-2.33.1.1.5.0 = STRING: "CS141 SNMP/WEB Adapter"
SNMPv2-SMI::mib-2.33.1.6.1.0 = Gauge32: 0
SNMPv2-SMI::mib-2.33.1.2.1.0 = INTEGER: 2
SNMPv2-SMI::mib-2.33.1.2.7.0 = INTEGER: 21
SNMPv2-SMI::mib-2.33.1.2.4.0 = INTEGER: 100
SNMPv2-SMI::mib-2.33.1.2.3.0 = INTEGER: 269
SNMPv2-SMI::mib-2.33.1.2.2.0 = INTEGER: 0
SNMPv2-SMI::mib-2.33.1.4.1.0 = INTEGER: 3
```
